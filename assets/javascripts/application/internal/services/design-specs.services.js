const getDesignSpecs = (callback) => { // eslint-disable-line no-unused-vars
  const query = `designspecs { name, tag, xdSpecs, xdBackup, trello, documentDescription, date }`

  $.when(
    $.ajax({
      url: `/api/strapi?q=${query}`,
      dataType: 'json',
      error: function (xhr, status, error) {
        console.error(`There was an error in the request: ${error}`)
      }
    }))
    .then(function (data) {
      callback(data)
    })
}
