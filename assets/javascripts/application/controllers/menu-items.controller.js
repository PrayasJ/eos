$(function () {
  /*
    Use this as follows:
    Add the class .js-select-current to the link you want to select
    it will check if `href = path` matches with the current path
    in the URL, and if it does, the item will get the class `.selected`
   */
  $('.js-select-current').addClass(function () {
    const itemRoute = $(this).attr('href')
    return window.location.pathname === itemRoute ? 'selected' : ''
  })

  /*
    Use this as follows:
    Add the class .js-select-current-parent to the link you want to select.
    It will check if the `href = path` in your item
    matches with parent section of the current path. For example:
    being located at localhost:3000/foo/bar, if your href is `/foo`
    the item will get the class `.selected`, but if the href was `/foo/bar`
    then it will be skipped and not be selected.
   */
  $('.js-select-current-parent').addClass(function () {
    const itemRoute = $(this).attr('href').replace('/', '')
    const currentRoute = window.location.pathname
    const currentRouteParent = currentRoute.split('/')
    return itemRoute === currentRouteParent[1] ? 'selected' : ''
  })

  makeSubmenuSectionVisible(submenuCollapse, submenuDropdownPosition) // eslint-disable-line no-undef
  $(window).resize(fn => {
    makeSubmenuSectionVisible(submenuCollapse, submenuDropdownPosition)// eslint-disable-line no-undef
  })
})

/*
  Use this as follows:
  Add the class .js-submenu-make-visible to the submenu nav you want to make visible.
  It will check if the `data-parent-menu = path` in the submenu
  matches with parent section of the current path. For example:
  being located at localhost:3000/foo/bar, if your data-parent-menu is `/foo`
  the submenu will become visible, but if the href was `/foo/bar`
  then it will stay hidden.
 */

const makeSubmenuSectionVisible = (submenu, submenuDopDown) => {
  $('.js-submenu-make-visible').addClass(function () {
    const pm = $(this).data('parent-menu') // pm => parent menu
    const currentRoute = window.location.pathname
    const currentRouteParent = currentRoute.split('/')

    submenuDopDown()
    submenu()
    return pm === currentRouteParent[1] ? 'visible js-submenu-visible' : ''
  })
}
