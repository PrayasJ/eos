const fs = require('fs')
const { readConfigFile } = require('./strapi-graphql-query')

/* Creates config.json bases on template if the files does not exist */
const createStrapiConfigFile = async () => {
  const createFile = async () => {

    const _template = {
      envVars: {
        dev: {
          strapiUrl: (process.env.EOS_STRAPI_SERVER_DEV || 'https://eos-strapi-dev.herokuapp.com'),
          user: (process.env.EOS_STRAPI_USERNAME_DEV || 'testing'),
          pass: (process.env.EOS_STRAPI_PASSWORD_DEV || '_testingStrapi_')
        },
        prod: {
          strapiUrl: null,
          user: null,
          pass: null
        }
      },
      useStrapiAs: "dev"
    }

    return fs.writeFileSync('config.json', JSON.stringify(_template, null, 2));
  }

  /* Check if config.json is created, if not, creates it with default values. */
  fs.access('config.json', fs.F_OK, async (err) => {
    if (err) {
      console.error(`Initial config file was not found, creating one ... `);

      if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'staging') {
        return await createFile().then(() => {
          return setStrapiOnServer()
        })
      } else {
        createFile()
      }

      return console.log('✅  Done')
    }
    return
  });
}

/* Switch the Strapi server URL command */
const switchStrapi = async setTo => {
  let data = await readConfigFile()

  /* Sets the object key/value for Strapi env */
  data.useStrapiAs = setTo

  /* Notify the user with the change of env */
  console.log(` 🔄  Switching Strapi server to ${setTo === 'dev' ? 'development' : 'production'}`)

  return fs.writeFileSync('config.json', JSON.stringify(data, null, 2));
}

/* When in production, set the config file values from server ENV Variables */
const setStrapiOnServer = async () => {
  let data = await readConfigFile()

  console.log('process.env.EOS_STRAPI_SERVER_PROD: ', process.env.EOS_STRAPI_SERVER_PROD);
  if (data.envVars.prod.strapiUrl === null) {
    /* Sets the object values for production */
    data.envVars.prod.strapiUrl = process.env.EOS_STRAPI_SERVER_PROD
    data.envVars.prod.user = process.env.EOS_STRAPI_USERNAME_PROD
    data.envVars.prod.pass = process.env.EOS_STRAPI_PASSWORD_PROD
    data.useStrapiAs = 'prod'

    return fs.writeFileSync('config.json', JSON.stringify(data, null, 2));
  }
}

module.exports = {
  switchStrapi,
  createStrapiConfigFile
}
