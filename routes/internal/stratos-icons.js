const express = require('express')
const router = express.Router()

/* GET home page. */
router.get('/', (req, res, next) => {
  if (process.env.NODE_ENV === 'staging' || process.env.NODE_ENV === 'production') {
    res.redirect('/')
  } else {
    res.render('internal/stratos-icons')
  }
})

module.exports = router
